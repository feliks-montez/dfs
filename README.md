# Distributed Filesystem

### How to Run
- Make sure to change the `dfs_root_dir` and `dfs_data_dir` settings to paths that exist on your computer.
- Open a python 2.7 shell and start NameNode:
```python
from namenode import NameNode
nn = NameNode()
```
- Open another python 2.7 shell and start DataNode:
```python
from datanode import DataNode
dn = DataNode()
```

### To Do
- transfer data over USB (3.0 and 3.1)
- program in low-level language for fast data transfer (C++, Java?) Looks like it won't matter because every high-level language handles USB with C++ libs
- perhaps send out broadcast and listen for incoming data from the correct node
- assign a port for the DFS protocol
- DataNodes must use as little RAM as possible to leave room for data analytics
- Have separate DataNode-to-NameNode NameNode-to-DataNode ports. Each waits for a specific response from the other node

### Notes
- Each node will only have 1 USB connection port
- Linux allows USB to function as Ethernet by default

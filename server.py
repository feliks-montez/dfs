import serial

class Server():

  def __init__(self):
    if __name__ == __main__:
      self.port = '/dev/ttyACM0'
      self.baudrate = 115200
      self.bytesize = Serial.EIGHTBITS
      self.connect()
      
      self.root_dir = '~/dfs'
  
  def connect(self):
    self.socket = serial.Serial(port=self.port, baudrate=self.baudrate)
    self.socket.open()
    
  def recieve_data(self):
    while self.socket.in_waiting > 0:
      chars = self.socket.read(3)
      string += chars
      if "EOF" in chars: # terminating characters; this may not work as expcted because the 3 chars could be offset
        break
    return string
    
  def send_data(self, data):
    self.socket.send(data)
    
  # TODO: create a data format to send files. maybe headers, etc. specify info like filename, file data, versions, etc.

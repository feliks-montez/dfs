import socket
import os
import json
import time
import thread

class NameNode():

  def __init__(self):
    print "Starting NameNode"
    
    self.settings = {
      'dfs_root_dir': 'file:///home/feliks/fs/',
      'dfs_data_dir': 'file:///home/feliks/fsdata/', # 'dfs:///data/mydata:4000'
      'dfs_port': 4000,
      'datanode_port': 4400,
      'max_nodes': 8,
      'namenode_address': 'localhost', #'192.168.1.39'
      'dfs_replication': 3
    }
  
    self.init_path(self.settings['dfs_root_dir'])
    self.init_path(self.settings['dfs_data_dir'])
    
    self.load_datanode_files()
    
    self.live_datanodes = []
    
    try:
      thread.start_new_thread(self.startup, ("Listening-Thread",))
    except:
      print "Error: Unable to start thread."
    
  def startup(self, threadName="Listening-Thread"):
    print "Listening on {0}:{1}".format(self.settings['namenode_address'], self.settings['dfs_port'])
    s = socket.socket()
    s.bind((self.settings['namenode_address'], self.settings['dfs_port']))
    s.listen(self.settings['max_nodes'])
    
    while True:
      connection, address = s.accept()
      client_ip = address[0]
      self.live_datanodes.append(client_ip)
      rcv = ""
      buf = connection.recv(1024) # if the message exceeds this size, it will not make it through the EOF if statement
      if len(buf) > 0:
        rcv += buf
        if "EOF" in buf:
          rcv = rcv.replace('EOF', '')
          self.handle_incoming_data(rcv)
          rcv = ""
    s.close()
    
  def load_datanode_files(self):
    prefix, path = self.settings['dfs_data_dir'].split("://")# or ['file', self.settings['dfs_data_dir']]
    if prefix == 'file':
      files_file = os.path.join(path, 'files')
      f = open(files_file, 'w+')
      if f.read() == "":
        f.write("[]")
        f.seek(0) # reset cursor back to beginning
      self.files = json.loads(f.read())
      
  def parse_datanodes_file(self):
    prefix, path = self.settings['dfs_data_dir'].split("://")
    f = open(os.path.join(path, self.settings['datanodes_file']))
    datanodes = f.read().strip().split()
    f.close()
    return datanodes
          
  def handle_incoming_data(self, data):
    try:
      data = json.loads(data)
      if "message" in data.keys():
        print data["message"]
    except: # data wasn't JSON
      print data
        
  def parse_json(self, json):
    json = json.loads(json)
    #files = open(
    if typeof(json) == typeof([]): # it's an array of files
      for _file in json:
        filename = _file["filename"]
  
  def send_json_to_datanode(self, _json, ip):
    self.send_data(json.dumps(_json), ip)
    
  def send_data_to_datanode(self, data, ip):
    s = socket.socket()
    s.connect((ip, self.settings['datanode_port']))
    s.send(data+"EOF")
    s.close()
  
  def copy_from_local(self, infile, outfile):
    infile = open(infile)
    data = [
      {
        "filename": f.name,
        "data": f.read()
      }
    ]
    data = json.loads(data) + "EOF"
    
    s = socket.socket()
    s.connect((self.determine_datanode_address(), self.settings['datanode_port']))
    s.send(data)
    s.close()
    
  def determine_datanode_address(self):
    most_free_space_datanode
    most_free_space = 0 # in MB
    for ip in self.live_datanodes:
      data = {
        "gimme": "free_space"
      }
      self.send_json(data)
      pass
    return 'localhost'
    
  def init_path(self, path):
    prefix, path = path.split("://")
    if prefix == 'file':
      if not os.path.exists(path):
        os.mkdir(path)

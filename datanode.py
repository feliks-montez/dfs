import os
import socket
import json
import thread

class DataNode():
  
  def __init__(self):
    print "Starting DataNode"
    self.settings = {
      'dfs_root_dir': 'file:///home/feliks/fs/',
      'dfs_data_dir': 'file:///home/feliks/fsdata/',
      'dfs_port': 4000,
      'datanode_port': 4400,
      'namenode_address': 'localhost'
    }
  
    self.init_path(self.settings['dfs_root_dir'])
    self.init_path(self.settings['dfs_data_dir'])
    
    prefix, path = self.settings['dfs_data_dir'].split("://") or ['file', self.settings['dfs_data_dir']]
#    if prefix == 'file':
#      files_file = os.path.join(path, 'files')
#      self.files = open(files_file, 'w+').read().split()
    
    try:
      thread.start_new_thread(self.startup, ("Listening-Thread",))
    except:
      print "Error: Unable to start thread."
    
  def mkdir(self, directory):
    prefix, path = self.settings['dfs_root_dir'].split("://")
    directory = os.path.join(path, directory)
    if os.path.exists(directory):
      return False
    else:
      os.mkdir(directory)
      return True
  
  def init_path(self, path):
    prefix, path = path.split("://")
    if prefix == 'file':
      if not os.path.exists(path):
        os.mkdir(path)
        
  def startup(self, threadName='Listening-Thread'):
    # connect to NameNode
    data = {
      "message": "DataNode connected"
    }
    self.send_json(data)
    
    # listen for NameNode
    print "Listening on {0}:{1}".format(self.get_ip(), self.settings['datanode_port'])
    s = socket.socket()
    s.bind((self.get_ip(), self.settings['datanode_port']))
    s.listen(1) # only listening for NameNode
    
    while True:
      connection, address = s.accept()
      rcv = ""
      buf = connection.recv(1024) # if the message exceeds this size, it will not make it through the EOF if statement
      if len(buf) > 0:
        rcv += buf
        if "EOF" in buf:
          rcv = rcv.replace('EOF', '')
          self.handle_incoming_data(rcv)
          rcv = ""
    s.close()
    
  def handle_incoming_data(self, data):
    try:
      data = json.loads(data)
      if "message" in data.keys():
        print data["message"]
      if "gimme" in data.keys():
        if data["gimme"] == "file":
          path = data["file"]
          f = self.open(path)
          # send back the file NameNode asked for
          data = {
            "hereyago": "file",
            "file": f.name,
            "data": f.read()
          }
          f.close()
        if data["gimme"] == "free_space":
          free_space = self.get_free_space_mb(self.settings['dfs_root_dir'])
          data = {
            "hereyago": "free_space",
            "free_space": free_space
          }
        # send NameNode what it asked for
        self.send_json(data)
    except: # data wasn't JSON
      print data
    
  def send_json(self, _json):
    self.send_data(json.dumps(_json))
    
  def send_data(self, data):
    s = socket.socket()
    s.connect((self.settings['namenode_address'], self.settings['dfs_port']))
    s.send(data+"EOF")
    s.close()
    
  def upload_file(self, _file):
    f = open(_file, 'r')
    dfs_file = self.open(_file, 'w+')
    dfs_file.write(f.read())
    json = {
      "filename": f.name,
      "data": f.read()
    }
    json = json.loads(json)
    f.close()
    dfs_file.close()
  
  def get_ip(self):
    return socket.gethostbyname(socket.gethostname())
    
  def get_free_space_mb(dirname):
    """Return folder/drive free space (in megabytes)."""
#    if platform.system() == 'Windows':
#        free_bytes = ctypes.c_ulonglong(0)
#        ctypes.windll.kernel32.GetDiskFreeSpaceExW(ctypes.c_wchar_p(dirname), None, None, ctypes.pointer(free_bytes))
#        return free_bytes.value / 1024 / 1024
#    else:
    st = os.statvfs(dirname)
    return st.f_bavail * st.f_frsize / 1024 / 1024
  
  def open(self, _file, mode='r'):
    f = open(os.path.join(self.settings['dfs_root_dir'].split("://")[1], _file), mode)
    return f
